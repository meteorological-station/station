const fetch = require('node-fetch');

const url = 'http://localhost:8085/add';

const fakeData = (prev, minLim, maxLim, min=-1, max=1) => {
	const rand = Math.random() * (max - min) + min;
	if (prev + rand < minLim || prev + rand > maxLim) {
		return prev - rand;
	}
	return prev + rand;
}

const sendFake = async (data) => {
	data.temperature = fakeData(data.temperature, 20, 30, 0.01, -0.01);
	data.humidity = fakeData(data.humidity, 70, 90, 0.01, -0.01);
	const urlParams = `?temperature=${data.temperature}&humidity=${data.humidity}`;
	const resp = await fetch(url + urlParams, {
		method: 'POST'
	})
	console.log(data, resp.status);
}


(async function main(){
	let data = {
		temperature: 25,
		humidity: 80
	};
	const timer = setInterval(async () => await sendFake(data), 5000);
}())